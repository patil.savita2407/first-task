from django.contrib import admin

from .models import Stream
from .models import Subject
from .models import Question_bank,Question,Complexity,Exam_creation,Syallbus_name

admin.site.register(Stream)
admin.site.register(Subject)
admin.site.register(Question_bank)
 
class QuestionAdmin(admin.ModelAdmin):
    #fields = ('option1', 'option2', 'option3','option4')
    list_display = [field.name for field in Question._meta.get_fields()]
admin.site.register(Question, QuestionAdmin)

def __str__(self): 
    	return self.list_display
        
class Exam_creationAdmin(admin.ModelAdmin):
    list_display = [field.name for field in Exam_creation._meta.get_fields()]
admin.site.register(Exam_creation, Exam_creationAdmin)

def __str__(self): 
    	return self.list_display

class Syallbus_nameAdmin(admin.ModelAdmin):
    fields = ('subject_id', 'syallbus')
    list_display = [field.name for field in Syallbus_name._meta.get_fields()]
admin.site.register(Syallbus_name, Syallbus_nameAdmin)

def __str__(self): 
    	return self.list_display